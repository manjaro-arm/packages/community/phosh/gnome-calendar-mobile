# Maintainer: Philip Müller <philm@manjaro.org>
# Contributor: Fabian Bornschein <fabiscafe-at-mailbox-dot-org>
# Contributor: Jan Alexander Steffens (heftig) <heftig@archlinux.org>
# Contributor: Jan de Groot <jgc@archlinux.org>

pkgname=gnome-calendar-mobile
pkgver=45.1
pkgrel=2
pkgdesc="Simple and beautiful calendar application designed to perfectly fit the PHOSH environment"
url="https://wiki.gnome.org/Apps/Calendar"
arch=(x86_64 aarch64)
license=(GPL)
depends=(
  dconf
  evolution-data-server
  geoclue
  geocode-glib-2
  glib2
  graphene
  gsettings-desktop-schemas
  gtk4
  hicolor-icon-theme
  libadwaita
  libedataserverui4
  libgweather-4
  libical
  libsoup3
  pango
)
makedepends=(
  git
  glib2-devel
  meson
)
optdepends=('gnome-control-center: Manage online accounts')
provides=("gnome-calendar=$pkgver")
conflicts=('gnome-calendar')
_commit=a40ddb658cd77cd3723e74926a80e5b4957a120d  # tags/45.1^0
source=("git+https://gitlab.gnome.org/GNOME/gnome-calendar.git#commit=$_commit"
         reduce-location-accuracy.diff
         # Mobian patches
         0001-gcal-window-make-it-fit-phone-screens.patch
)
sha256sums=('SKIP'
            '190f627d7a518f9104d54a4a15f3d6c8b14a08472ef3b28e63d15328d7e106e1'
            'e2778e99c1bbdcbf4bef9c5abb85e47f940baaf673de0198d146a2075535f459')

pkgver() {
  cd gnome-calendar
  git describe --tags | sed 's/[^-]*-g/r&/;s/-/+/g'
}

prepare() {
  cd gnome-calendar
  
  # Reduce accuracy of location requests
  git apply -3 ../reduce-location-accuracy.diff
  
  local src
  for src in "${source[@]}"; do
      src="${src%%::*}"
      src="${src##*/}"
      [[ $src = *.patch ]] || continue
      echo "Applying patch $src..."
      patch -Np1 < "../$src" || true
  done  
}

build() {
  arch-meson gnome-calendar build
  meson compile -C build
}

check() {
  meson test -C build --print-errorlogs
}

package() {
  meson install -C build --destdir "$pkgdir"
  echo "X-Purism-FormFactor=Workstation;Mobile;" >> "$pkgdir/usr/share/applications/org.gnome.Calendar.desktop"
}

# vim:set sw=2 sts=-1 et:

